import Axios, { AxiosInstance } from 'axios';
import Config from '@/config/mantis';

export default class Http {
  public axios: AxiosInstance;

  constructor() {
    this.axios = Axios.create({
      timeout: Config.timeout,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        Accept: 'application/json',
      },
      baseURL: Config.baseURL,
    });

    this.axios.interceptors.response.use(
      (res) => res,
      (err) => {
        if (err.response && err.response.status === 401) {
          window.location.href = '/auth/login';
        }
        return Promise.reject(err);
      },
    );

    this.axios.interceptors.request.use(async (config) => {
      try {
        const token = await this.getAccessToken();
        if (token) {
          // eslint-disable-next-line no-param-reassign
          config.headers.Authorization = `Bearer ${token}`; // User is logged in. Set auth header on all requests
        }
        return config;
      } catch (err) {
        // eslint-disable-next-line no-console
        console.error(err);
        return config;
      }
    }); // No logged-in user: don't set auth header
  }

  // eslint-disable-next-line class-methods-use-this
  public async getAccessToken() {
    const Cognito = document.querySelector('lar-cognito-config') as HTMLLarCognitoConfigElement;
    if (!Cognito) {
      throw new Error('lar-cognito-config not loaded');
    }
    const token = await Cognito.getAccessToken();
    return token;
  }
}
