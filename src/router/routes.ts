import { RouteRecordRaw } from 'vue-router';
import HomeRoutes from '@/modules/home/routes';
import AdminRoutes from '@/modules/admin/routes';
import AuthRoutes from '@/modules/auth/routes';
import TenantRoutes from '@/modules/tenant/routes';

const routes: RouteRecordRaw[] = [
  ...HomeRoutes,
  ...AuthRoutes,
  ...AdminRoutes,
  ...TenantRoutes,
];

export default routes;
