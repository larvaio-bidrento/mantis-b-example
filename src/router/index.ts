import { createWebHistory, createRouter } from 'vue-router';
import generalConfig from '@/config/general';
import routes from './routes';

const router = createRouter({
  history: createWebHistory(generalConfig.routerBaseUrl),
  routes,
});
export default router;
