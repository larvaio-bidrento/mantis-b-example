/* eslint-disable no-alert */
/* eslint-disable class-methods-use-this */
import { Vue } from 'vue-class-component';
import { inject } from 'vue';
import Http from '@/providers/http';
import { useLoading } from 'vue3-loading-overlay';
// Import stylesheet
import 'vue3-loading-overlay/dist/vue3-loading-overlay.css';
// Init plugin

export { Options as Component } from 'vue-class-component';

export class ComponentBase extends Vue {
  protected readonly http: Http = inject('http') as Http;

  private loader = useLoading();

  protected get axios() {
    return this.http.axios;
  }

  public async beforeDestory(): Promise<void> {
    // make sure we dismiss the loading modal
    return this.loadingDone();
  }

  protected async setLoading(): Promise<void> {
    this.loader.show();
  }

  protected async loadingDone(): Promise<void> {
    this.loader.hide();
  }

  protected async logout() {
    const Cognito = document.querySelector('lar-cognito-config') as HTMLLarCognitoConfigElement;
    if (Cognito) {
      await Cognito.logout()
        .catch((err) => console.error(err));
    }
    this.$router.replace({ name: 'auth.login' });
  }
}
