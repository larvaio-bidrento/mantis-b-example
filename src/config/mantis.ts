import GeneralConf from './general';

export default {
  baseURL: process.env.VUE_APP_MANTIS_URL || `https://mantis.larva.io/api/orgs/${GeneralConf.orgId}`,
  timeout: 15000,
};
