export default {
  region: process.env.VUE_APP_COGNITO_REGION || 'eu-north-1',
  userPoolId: process.env.VUE_APP_COGNITO_USER_POOL_ID || 'eu-north-1_yhzdUogMe',
  userPoolWebClientId: process.env.VUE_APP_COGNITO_CLIENT_ID || '6f5v563vrjrfm63htgsa11rerc',
  usernameAttribute: process.env.VUE_APP_USERNAME_ATTRIBUTE || 'email',
  storage: process.env.VUE_APP_COGNITO_STORAGE || 'session',
};
