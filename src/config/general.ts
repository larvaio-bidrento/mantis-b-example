export default {
  routerBaseUrl: process.env.BASE_URL,
  deviceProxy: process.env.VUE_APP_BROKER_URL || 'wss://broker.larva.io',
  deviceTimeout: process.env.VUE_APP_BROKER_TIMEOUT || 8000,
  videoProxy: process.env.VUE_APP_VIDEO_URL || 'wss://video-broker.larva.io/',
  orgId: process.env.VUE_APP_MANTIS_ORGID || '102883c4-b0c2-48f0-b8f3-30a1e133a8a6',
  serviceConnectionExternalId: process.env.VUE_APP_MANTIS_SERVICE_CONNECTION_EXTERNAL_ID || '624924a2-1376-491c-ab5c-7813ebafdad3',
};
