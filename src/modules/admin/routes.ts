/* eslint-disable @typescript-eslint/no-explicit-any */
import { RouteRecordRaw } from 'vue-router';
import MainLayout from '@/layouts/main.vue';
import AdminLayout from './layouts/admin.vue';

export default [
  {
    path: '/admin',
    redirect: '/admin/pages/home',
    component: MainLayout,
    children: [
      {
        path: 'pages',
        redirect: 'home',
        component: AdminLayout,
        children: [
          {
            path: 'home',
            name: 'admin.home',
            component: (): Promise<any> => import('./pages/home.vue'),
          },
          {
            path: 'agreements',
            name: 'admin.agreements',
            component: (): Promise<any> => import('./pages/agreements.vue'),
          },
          {
            path: 'agreements/:id',
            name: 'admin.agreement',
            component: (): Promise<any> => import('./pages/agreement.vue'),
          },
          {
            path: 'logs',
            name: 'admin.logs',
            component: (): Promise<any> => import('./pages/logs.vue'),
          },
        ],
      },
    ],
  },
] as RouteRecordRaw[];
