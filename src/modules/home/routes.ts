/* eslint-disable @typescript-eslint/no-explicit-any */
import { RouteRecordRaw } from 'vue-router';
import MainLayout from '@/layouts/main.vue';

export default [
  {
    path: '/',
    redirect: '/home',
    component: MainLayout,
    children: [
      {
        path: 'home',
        name: 'home',
        component: (): Promise<any> => import('./pages/home.vue'),
      },
      {
        path: '404',
        name: '404',
        component: (): Promise<any> => import('./pages/404.vue'),
      },
    ],
  },
  {
    path: '/:catchAll(.*)',
    redirect: '/404',
  },
] as RouteRecordRaw[];
