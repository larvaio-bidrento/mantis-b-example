/* eslint-disable @typescript-eslint/no-explicit-any */
import { RouteRecordRaw } from 'vue-router';
import MainLayout from '@/layouts/main.vue';
import TenantLayout from './layouts/tenant.vue';

export default [
  {
    path: '/tenant',
    redirect: '/tenant/pages/home',
    component: MainLayout,
    children: [
      {
        path: 'pages',
        redirect: 'home',
        component: TenantLayout,
        children: [
          {
            path: 'home',
            name: 'tenant.home',
            component: (): Promise<any> => import('./pages/home.vue'),
          },
          {
            path: 'agreements/:agreementUUID/unit/:id',
            name: 'tenant.agreement.unit',
            component: (): Promise<any> => import('./pages/unit.vue'),
          },
        ],
      },
    ],
  },
] as RouteRecordRaw[];
