/* eslint-disable @typescript-eslint/no-explicit-any */
import { RouteRecordRaw } from 'vue-router';
import MainLayout from '@/layouts/main.vue';
import LoginLayout from './layouts/login.vue';

export default [
  {
    path: '/auth',
    redirect: '/auth/pages/login',
    component: MainLayout,
    children: [
      {
        path: 'pages',
        redirect: 'login',
        component: LoginLayout,
        children: [
          {
            path: 'login',
            name: 'auth.login',
            component: (): Promise<any> => import('./pages/login.vue'),
          },
        ],
      },
    ],
  },
] as RouteRecordRaw[];
