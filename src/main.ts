import { createApp } from 'vue';
import { LarvaWebcomponents } from '@larva.io/webcomponents-vue';
import { LarvaWebcomponentsCognitoLogin } from '@larva.io/webcomponents-cognito-login-vue';
import App from './App.vue';
import router from './router';
import Http from './providers/http';
import GeneralConfig from './config/general';

const http = new Http();

const app = createApp(App)
  .use(LarvaWebcomponentsCognitoLogin)
  .use(LarvaWebcomponents, { videoproxyURL: GeneralConfig.videoProxy })
  .use(router)
  .provide('http', http);

router.isReady()
  .then(() => customElements.whenDefined('lar-cognito-config')) // @TODO: if released use isReady on a plugin, since vue is not waiting for async plugin install method
  .then(() => customElements.whenDefined('lar-app')) // @TODO: if released use isReady on a plugin, since vue is not waiting for async plugin install method
  .then(() => app.mount('#app'))
  .catch((err: Error) => {
  // eslint-disable-next-line no-alert
    alert(err.message);
  });
